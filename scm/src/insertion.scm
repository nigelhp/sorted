(require-extension srfi-1) ; fold

(define (sort values)
  (letrec ((insert (lambda (x xs)
                     (cond ((null? xs) (list x))
                           ((<= x (car xs)) (cons x xs))
                           (else (cons (car xs)
                                       (insert x (cdr xs))))))))
    (fold insert '() values)))
