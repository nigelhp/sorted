# Sorted

Implementing some of the simpler sorting algorithms via a series of katas in
different languages.

## Languages
* Erlang
* Java
* Scala
* Scheme
* Standard ML

## Algorithms
* Insertion Sort ([Wikipedia](https://en.wikipedia.org/wiki/Insertion_sort))
