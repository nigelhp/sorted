package com.nigelhp.sorted

object Insertion {
  def sort(values: List[Int]): List[Int] =
    values.foldLeft(List.empty[Int])(insert)

  private def insert(ascending: List[Int], n: Int): List[Int] =
    ascending match {
      case Nil => List(n)
      case h :: t if n <= h => n :: h :: t
      case h :: t => h :: insert(t, n)
    }
}
