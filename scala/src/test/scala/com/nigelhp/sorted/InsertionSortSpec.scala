package com.nigelhp.sorted

import org.scalatest.{FreeSpec, Matchers}
import org.scalatest.prop.TableDrivenPropertyChecks

class InsertionSortSpec extends FreeSpec with Matchers with TableDrivenPropertyChecks {

  private trait Fixture {
    val sort = Insertion.sort _

    val examples = Table(
      ("input", "expectedOutput"),
      (List.empty[Int], List.empty[Int]),
      (List(42), List(42)),
      (List(-42, 42), List(-42, 42)),
      (List(42, -42), List(-42, 42)),
      (List(42, 42), List(42, 42)),
      (List(-42, 0, 42), List(-42, 0, 42)),
      (List(-42, 42, 0), List(-42, 0, 42)),
      (List(0, -42, 42), List(-42, 0, 42)),
      (List(0, 42, -42), List(-42, 0, 42)),
      (List(42, -42, 0), List(-42, 0, 42)),
      (List(42, 0, -42), List(-42, 0, 42)))
  }

  "Sort" - {
    "should sort the input into ascending order" in new Fixture {
      forAll(examples) { (input, expectedOutput) =>
        sort(input) shouldBe expectedOutput
      }
    }
  }
}
