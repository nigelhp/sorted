package com.nigelhp.sorted

/*
 * Use the inbuilt library implementation as a "test oracle".
 */
class SortTestOracleSpec extends AbstractPropertySpec {

  private trait Fixture {
    val sort = Insertion.sort _
  }

  "Sort" - {
    "returns the same result as the inbuilt library implementation" in new Fixture {
      forAll { (values: List[Int]) =>
        sort(values) shouldBe values.sorted
      }
    }
  }
}
