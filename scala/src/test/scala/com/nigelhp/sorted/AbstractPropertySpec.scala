package com.nigelhp.sorted

import org.scalatest.prop.GeneratorDrivenPropertyChecks
import org.scalatest.{FreeSpec, Matchers}

abstract class AbstractPropertySpec extends FreeSpec with Matchers with GeneratorDrivenPropertyChecks
