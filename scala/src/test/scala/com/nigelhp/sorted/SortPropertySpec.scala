package com.nigelhp.sorted

import org.scalacheck.Gen

class SortPropertySpec extends AbstractPropertySpec {

  private object LibrarySort {
    def ascending(values: List[Int]): List[Int] =
      values.sorted

    def descending(values: List[Int]): List[Int] =
      values.sorted(Ordering[Int].reverse)
  }

  private trait Fixture {
    val sort = Insertion.sort _

    def assertHasOrderedPairs(values: List[Int]): Unit =
      values.sliding(2).foreach(assertIsOrdered)

    /*
     * Note the assumption that candidatePair contains 0..2 elements.
     */
    private def assertIsOrdered(candidatePair: List[Int]): Unit =
      candidatePair match {
        case h :: s :: Nil => h should be <= s
        case _ :: _ :: _ => fail(s"Unexpected candidate is not a pair [$candidatePair]")
        case _ =>
      }

    def assertIsPermutationOf(x: List[Int], y: List[Int]) = {
      x.diff(y) shouldBe empty
      y.diff(x) shouldBe empty
    }
  }

  "Sort" - {
    /*
     * This is sufficient by itself to validate an implementation.
     * Note however that it is a composite of two properties:
     * - the result should be a permutation of / contain the same elements as the original
     * - the result should be ordered such that each element is less than or equal to its following neighbour
     */
    "returns a sequence consisting of all the original values ordered such that each value is less than or equal to the following value" in new Fixture {
      forAll { (values: List[Int]) =>
        val sortedValues = sort(values)

        assertIsPermutationOf(sortedValues, values)
        assertHasOrderedPairs(sortedValues)
      }
    }

    /*
     * The following properties are useful to consider when thinking about a solution,
     * but are easily subverted (on an individual basis anyway) by a malicious implementation.
     */

    // an implementation can simply reverse the input list to pass this
    "reverses the original sequence when it has descending order" in new Fixture {
      forAll { (values: List[Int]) =>
        val descendingValues = LibrarySort.descending(values)

        sort(descendingValues) shouldBe descendingValues.reverse
      }
    }

    // an implementation can simply return the input list to pass this
    "returns the original sequence when it has ascending order" in new Fixture {
      forAll  { (values: List[Int]) =>
        val ascendingValues = LibrarySort.ascending(values)

        sort(ascendingValues) shouldBe ascendingValues
      }
    }

    /*
     * An example of the "Different paths, same destination" strategy from:
     * https://fsharpforfunandprofit.com/posts/property-based-testing-2/#different-paths
     *
     * Again this is insufficient by itself, as an implementation could simply return the empty list.
     *
     * Note that the above article does not deal with the fact that Int.MinValue * -1 == Int.MinValue
     * as a result of overflow, and so for simplicity we exclude generated lists containing Int.MinValue here.
     */
    "different paths arrive at the same destination" in new Fixture {
      val intGreaterThanMinValue = Gen.listOf(Gen.choose(Int.MinValue + 1, Int.MaxValue))

      forAll(intGreaterThanMinValue) { (values: List[Int]) =>
        whenever(!values.contains(Int.MinValue)) {
          val negateThenSort = sort(values.map(_ * -1))
          val sortNegateAndReverse = sort(values).map(_ * -1).reverse

          negateThenSort shouldBe sortNegateAndReverse
        }
      }
    }
  }
}
