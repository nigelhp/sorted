package com.nigelhp.sorted;

import org.hamcrest.Matcher;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import java.util.Collection;
import java.util.List;

import static java.util.Arrays.asList;
import static java.util.Collections.emptyList;
import static java.util.Collections.singletonList;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.emptyCollectionOf;

@RunWith(Parameterized.class)
public class InsertionTest {

    @Parameters
    public static Collection<Object[]> data() {
        return asList(new Object[][] {
                // input, matchesExpectation
                {emptyList(), emptyCollectionOf(Integer.class)},
                {singletonList(42), contains(42)},
                {asList(-42, 42), contains(-42, 42)},
                {asList(42, -42), contains(-42, 42)},
                {asList(42, 42), contains(42, 42)},
                {asList(-42, 0, 42), contains(-42, 0, 42)},
                {asList(-42, 42, 0), contains(-42, 0, 42)},
                {asList(0, -42, 42), contains(-42, 0, 42)},
                {asList(0, 42, -42), contains(-42, 0, 42)},
                {asList(42, -42, 0), contains(-42, 0, 42)},
                {asList(42, 0, -42), contains(-42, 0, 42)}
        });
    }

    private final List<Integer> input;
    private final Matcher<List<Integer>> matchesExpectation;

    public InsertionTest(List<Integer> input, Matcher<List<Integer>> matchesExpectation) {
        this.input = input;
        this.matchesExpectation = matchesExpectation;
    }

    @Test
    public void sortedInputMatchesExpectation() {
        assertThat(new Insertion().sort(input), matchesExpectation);
    }
}
