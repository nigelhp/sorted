package com.nigelhp.sorted;

import com.pholser.junit.quickcheck.Property;
import com.pholser.junit.quickcheck.generator.InRange;
import com.pholser.junit.quickcheck.runner.JUnitQuickcheck;
import org.junit.runner.RunWith;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

@RunWith(JUnitQuickcheck.class)
public class SortProperties {
    /*
     * This is sufficient by itself to validate an implementation.
     * Note however that it is a composite of two properties:
     * - the result should contain the same elements as the original
     * - the result should be ordered such that each element is less than or equal to its following neighbour
     */
    @Property
    public void sortReturnsSameElementsInAscendingOrder(List<Integer> values) {
        List<Integer> ascending = new Insertion().sort(values);

        assertContainsTheSameElements(ascending, values);

        for (int i = 0; i < ascending.size() - 1; i++) {
            assertThat(ascending.get(i), lessThanOrEqualTo(ascending.get(i + 1)));
        }
    }

    /*
     * Use the inbuilt library implementation as a "test oracle".
     *
     * Note the care taken in librarySort to ensure that the generated List (values) is not mutated by the
     * JDK sort.  Otherwise, any test failure messages can be very confusing / misleading.
     */
    @Property
    public void sortReturnsSameResultAsTestOracle(List<Integer> values) {
        List<Integer> ascending = new Insertion().sort(values);

        assertContainsInOrder(ascending, librarySortAscending(values));
    }

    /*
     * The following properties are useful to consider when thinking about a solution,
     * but are easily subverted (on an individual basis anyway) by a malicious implementation.
     */

    // an implementation can simply reverse the input list to pass this
    @Property
    public void sortReversesOriginalSequenceWhenItHasDescendingOrder(List<Integer> values) {
        List<Integer> descending = librarySortDescending(values);

        assertContainsInOrder(new Insertion().sort(descending), reverse(descending));
    }

    // an implementation can simply return the input list to pass this
    @Property
    public void sortReturnsOriginalSequenceWhenItHasAscendingOrder(List<Integer> values) {
        List<Integer> ascending = librarySortAscending(values);

        assertContainsInOrder(new Insertion().sort(ascending), ascending);
    }

    /*
     * An example of the "Different paths, same destination" strategy from:
     * https://fsharpforfunandprofit.com/posts/property-based-testing-2/#different-paths
     *
     * Again this is insufficient by itself, as an implementation could simply return the empty list.
     *
     * Note that the above article does not deal with the fact that Integer.MIN_VALUE * -1 == Integer.MIN_VALUE
     * as a result of overflow, and so for simplicity we restrict the range of generated integers here.
     *
     * Curiously JUnitQuickcheck itself did not discover the significance of Integer.MIN_VALUE despite repeated
     * evaluations.  By contrast, property-based testing frameworks in other languages have tended to start at boundary
     * values and discover this straight away.
     */
    @Property
    public void sortDifferentPathsArriveAtSameDestination(
            List<@InRange(min = (Integer.MIN_VALUE + 1) + "", max = Integer.MAX_VALUE + "") Integer> values) {
        List<Integer> negatedThenSorted = new Insertion().sort(values.stream().map(n -> n * -1).collect(Collectors.toList()));
        List<Integer> sortedThenNegated = (new Insertion().sort(values)).stream().map(n -> n * -1).collect(Collectors.toList());

        assertContainsInOrder(negatedThenSorted, reverse(sortedThenNegated));
    }

    /*
     * Note that the Hamcrest IsIterableContainingInAnyOrder & IsIterableContainingInOrder matchers do no support
     * empty sequences.  We hide the ugly conditional in helpers below.
     *
     * An alternative approach would have been to specify a min size of 1 on the List generator, and rely on a unit
     * test for the empty list scenario.  Unfortunately, both the min and max have to specified if overriding the
     * default collection sizing behaviour, and we didn't really want to change the default behaviour that varies max
     * over the length of a test run.
     */

    private static void assertContainsTheSameElements(List<Integer> actual, List<Integer> expected) {
        if (expected.isEmpty()) {
            assertThat(actual, emptyCollectionOf(Integer.class));
        } else {
            assertThat(actual, containsInAnyOrder(elementsOf(expected)));
        }
    }

    private static void assertContainsInOrder(List<Integer> actual, List<Integer> expected) {
        if (expected.isEmpty()) {
            assertThat(actual, emptyCollectionOf(Integer.class));
        } else {
            assertThat(actual, contains(elementsOf(expected)));
        }
    }

    private static List<Integer> librarySortAscending(List<Integer> values) {
        return librarySort(values, Comparator.naturalOrder());
    }

    private static List<Integer> librarySortDescending(List<Integer> values) {
        return librarySort(values, Comparator.reverseOrder());
    }

    /*
     * Note that the JDK implementation of sort performs an in-place update on the collection.
     * We therefore take a copy of the supplied List to avoid mutating the client's List via aliasing.
     *
     * Without this, if the List has been generated by JUnitQuickcheck any failure messages will be confusing /
     * misleading, because it prints the value it believes was supplied to the test, and is unaware that the test
     * has mutated its contents.
     */
    private static List<Integer> librarySort(List<Integer> values, Comparator<Integer> ordering) {
        List<Integer> clone = copy(values);
        clone.sort(ordering);
        return clone;
    }

    private static List<Integer> reverse(List<Integer> values) {
        List<Integer> clone = copy(values);
        Collections.reverse(clone);
        return clone;
    }

    private static Integer[] elementsOf(List<Integer> values) {
        return values.toArray(new Integer[values.size()]);
    }

    private static List<Integer> copy(List<Integer> values) {
        return new ArrayList<>(values);
    }
}
