package com.nigelhp.sorted;

import java.util.ArrayList;
import java.util.List;

public class Insertion {
    /*
     * Note that we build a mutable result list.  This is safe, as it is not exposed until complete.
     *
     * Note that the Collector model requires both an Accumulator (insertAscending in our case) and a Combiner,
     * as it is designed with support for parallel operations in mind.  As we are modelling a sequential algorithm,
     * we have therefore avoided using values.stream.collect.
     */
    public List<Integer> sort(List<Integer> values) {
        List<Integer> result = new ArrayList<>();
        clone(values).forEach(n -> insertAscending(n, result));
        return result;
    }

    /*
     * This is in effect a BiConsumer<Integer, List<Integer>>.
     */
    private static void insertAscending(Integer value, List<Integer> values) {
        for (int i = 0; i < values.size(); i++) {
            if (values.get(i).compareTo(value) > 0) {
                values.add(i, value);
                return;
            }
        }

        // handle values.isEmpty or value > values.stream.max
        values.add(value);
    }

    private static List<Integer> clone(List<Integer> values) {
        return new ArrayList<>(values);
    }
}
