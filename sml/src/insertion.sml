
local
    fun insert(n, []) = [n]
      | insert(n, h :: t) =
        if n <= h
        then n :: h :: t
        else h :: insert(n, t)
in                        
fun sort ns =
    foldl insert [] ns
end
