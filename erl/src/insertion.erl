-module(insertion).

%% API exports
-export([sort/1]).

%%====================================================================
%% API functions
%%====================================================================
-spec sort(list()) -> list().
sort(List) ->
    lists:foldl(fun(Elem, Acc) -> insert(Elem, Acc) end, [], List).

%%====================================================================
%% Internal functions
%%====================================================================
-spec insert(term(), list()) -> list().
insert(Elem, []) ->
    [Elem];
insert(Elem, [H|T]) when Elem =< H ->
    [Elem, H|T]; 
insert(Elem, [H|T]) ->
    [H|insert(Elem, T)].
