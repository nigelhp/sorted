-module(prop_sort).

-include_lib("proper/include/proper.hrl").

%%%%%%%%%%%%%%%%%%
%%% Properties %%%
%%%%%%%%%%%%%%%%%%

%% This is sufficient by itself to validate an implementation.
prop_is_permutation_of_original_and_has_ascending_order() ->
    ?FORALL(In, list(integer()),
           begin
               Out = insertion:sort(In),
               is_permutation(In, Out) andalso has_ascending_order(Out)
           end).

%% Uses library implementation lists:sort/1 as a "test oracle".
prop_matches_test_oracle() ->
    ?FORALL(List, list(integer()), 
            insertion:sort(List) =:= lists:sort(List)).

 
%% The following properties are useful to consider when thinking about a 
%% solution, but are easily subverted (on an individual basis anyway) by a 
%% malicious implementation.

%% an implementation can simply reverse the input list to pass this
prop_reverses_original_when_ordered_descending() ->
    ?FORALL(List, list(integer()),
            begin
                Descending = library_sort_descending(List),
                insertion:sort(Descending) =:= lists:reverse(Descending)
            end).

%% an implementation can simply return the input list to pass this
prop_returns_original_when_ordered_ascending() ->
    ?FORALL(List, list(integer()),
            begin
                Ascending = lists:sort(List),
                insertion:sort(Ascending) =:= Ascending
            end).

%% An example of the "Different paths, same destination" strategy from:
%% https://fsharpforfunandprofit.com/posts/property-based-testing-2/#different-paths
%%
%% Again this is insufficient by itself, as an implementation can simply return
%% the empty list.
%%
%% Note that Erlang supports arbitrarily large ints (auto converting to bignums).
%% There is no Int.MinValue to worry about negating (as with Scala for example).
prop_different_paths_same_destination() -> 
    ?FORALL(List, list(integer()),
           begin
               Negate = fun(N) -> N * -1 end,
               NegateThenSort = insertion:sort(lists:map(Negate, List)),
               SortNegateAndReverse = lists:reverse(lists:map(Negate, insertion:sort(List))),
               NegateThenSort =:= SortNegateAndReverse
           end).


%%%%%%%%%%%%%%%
%%% Helpers %%%
%%%%%%%%%%%%%%%

is_permutation(List1, List2) ->
    List1 -- List2 =:= [] andalso List2 -- List1 =:= [].

has_ascending_order([H, N|T]) ->
    H =< N andalso has_ascending_order([N|T]);
has_ascending_order(_) ->
    true.

library_sort_descending(List) ->
    lists:sort(fun(A, B) -> A > B end, List).
