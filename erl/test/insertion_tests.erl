-module(insertion_tests).

-include_lib("eunit/include/eunit.hrl").

%% Note that a list of ints in the range of ASCII printable chars will print
%% to test output as a string.  To avoid potentially confusing test output
%% like 'expected: [] got: "*"' (where Dec 42 = Chr *) avoid sample values 
%% from this range.
sort_ascending_test_() ->
    [?_assertEqual([], insertion:sort([])),
     ?_assertEqual([1], insertion:sort([1])),
     ?_assertEqual([-1, 1], insertion:sort([-1, 1])),
     ?_assertEqual([-1, 1], insertion:sort([1, -1])),
     ?_assertEqual([1, 1], insertion:sort([1, 1])),
     ?_assertEqual([-1, 0, 1], insertion:sort([-1, 0, 1])),
     ?_assertEqual([-1, 0, 1], insertion:sort([-1, 1, 0])),
     ?_assertEqual([-1, 0, 1], insertion:sort([0, -1, 1])),
     ?_assertEqual([-1, 0, 1], insertion:sort([0, 1, -1])),
     ?_assertEqual([-1, 0, 1], insertion:sort([1, -1, 0])),
     ?_assertEqual([-1, 0, 1], insertion:sort([1, 0, -1]))].
